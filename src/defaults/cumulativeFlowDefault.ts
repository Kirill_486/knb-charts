// Google charts

import { IGoogleChartsColumn, IGoogleChartsRow } from "src/types/googleCharts";

const cumulativeFlowColumns: IGoogleChartsColumn[] = [
    {
        label: 'days',
        type: 'date'
    },
    {
        label: 'List1',
        type: 'number'
    },
    {
        type: 'string',
        role: 'tooltip',
        p: {
            'html': true
        }

    },
    {
        label: 'List2',
        type: 'number'
    },
    {
        type: 'string',
        role: 'tooltip',
        p: {
            'html': true
        }

    }
    
];

const createCustomTooltip = (date: string, list: string, cards: number) => {
    return `
        <div style="padding:5px 5px 5px 5px;">
            <span>${date}</span><br/>
            <span>--------</span><br/>
            <span>List:${list}</span><br/>
            <span>Cards:${cards}</span><br/>
        </div>
        `
};

const cumulativeFlowData: IGoogleChartsRow[] = [
    [new Date(2019, 2, 1), 1, createCustomTooltip('1.2.2019', 'List1', 1), 2, createCustomTooltip('1.2.2019', 'List2', 2)],
    [new Date(2019, 2, 2), 3, createCustomTooltip('2.2.2019', 'List1', 3), 4, createCustomTooltip('2.2.2019', 'List2', 4)],
    [new Date(2019, 2, 3), 6, createCustomTooltip('3.2.2019', 'List1', 6), 8, createCustomTooltip('3.2.2019', 'List2', 8)],
    [new Date(2019, 2, 4), 12, createCustomTooltip('4.2.2019', 'List1', 12), 16, createCustomTooltip('4.2.2019', 'List2', 16)],
    [new Date(2019, 2, 5), 24, createCustomTooltip('5.2.2019', 'List1', 24), 32, createCustomTooltip('5.2.2019', 'List2', 32)],
    [new Date(2019, 2, 6), 48, createCustomTooltip('6.2.2019', 'List1', 48), 64, createCustomTooltip('6.2.2019', 'List2', 64)],
    [new Date(2019, 2, 7), 96, createCustomTooltip('7.2.2019', 'List1', 96), 128, createCustomTooltip('7.2.2019', 'List2', 128)],
    [new Date(2019, 2, 8), 192, createCustomTooltip('8.2.2019', 'List1', 192), 256, createCustomTooltip('8.2.2019', 'List2', 256)],
    [new Date(2019, 2, 9), 96, createCustomTooltip('9.2.2019', 'List1', 96), 512, createCustomTooltip('9.2.2019', 'List2', 512)],
    [new Date(2019, 2, 10), 48, createCustomTooltip('10.2.2019', 'List1', 48), 256, createCustomTooltip('10.2.2019', 'List2', 256)],
    [new Date(2019, 2, 11), 24, createCustomTooltip('11.2.2019', 'List1', 24), 128, createCustomTooltip('11.2.2019', 'List2', 128)],
    [new Date(2019, 2, 12), 12, createCustomTooltip('12.2.2019', 'List1', 12), 64, createCustomTooltip('12.2.2019', 'List2', 64)]
]

export const googleCumulativeData = [cumulativeFlowColumns, ...cumulativeFlowData];

// ChartJs charts

export const chartjsLabels = [
    '01.02.2019',
    '02.02.2019',
    '03.02.2019',
    '04.02.2019',
    '05.02.2019',
    '06.02.2019',
    '07.02.2019',
    '08.02.2019',
    '09.02.2019',
    '10.02.2019',
    '11.02.2019',
    '12.02.2019'    
];

export const chartjsDatasets = [
    {
        label: 'List1',
        fillColor: "rgba(0,0,0,0)",
        strokeColor: "rgba(220,220,220,1)",
        pointColor: "rgba(200,122,20,1)",
        data: [2, 4, 8, 16, 32, 64, 128, 256, 512, 256, 128, 64]
    }, 
    {
        label: 'List2',
        fillColor: "rgba(0,0,0,0)",
        strokeColor: "rgba(220,220,220,1)",
        pointColor: "rgba(200,122,20,1)",
        data: [1, 3, 6, 12, 24, 48, 96, 192, 96, 48, 24, 12]
    }, 
];