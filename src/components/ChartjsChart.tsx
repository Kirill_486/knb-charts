import * as React from 'react';
import {Line} from 'react-chartjs-2';
import { chartjsLabels, chartjsDatasets } from 'src/defaults/cumulativeFlowDefault';

const chartData = {
    labels: chartjsLabels,
    datasets: chartjsDatasets
}

const chartOptions = {
    tooltips: {
        enabled: false,
        custom: (tooltipModel: any) => {
            // tslint:disable-next-line:no-console
            console.log(tooltipModel);
            return (
                <div>Jopa</div>
            )
        }
    }
}

export const ChartjsChart = () => {
    return (
        <div className="chartjs-line__container">
            <h1>ChartJs chart</h1>
            <Line 
                data={chartData}
                height={80}
                options={chartOptions}
            />
        </div>
    )
}
