// Google charts column

import { IGoogleChartsColumn } from "src/types/googleCharts";

const productivityColumns: IGoogleChartsColumn[] = [
    {
        label: 'users',
        type: 'string'
    },
    {
        label: 'Done',
        type: 'number',
    },
    {
        type: 'string',
        role: 'tooltip',
        p: {
            'html': true
        }
    },
    {
        type: 'string',
        role: 'style'
    },
    {
        label: 'Overdue',
        type: 'number'
    },
    {
        type: 'string',
        role: 'tooltip',
        p: {
            'html': true
        }
    },
    {
        type: 'string',
        role: 'style'
    },
];

const createCustomTooltip = (
    done: number,
    overdue: number
) => {
    return `
        <div style="padding:5px 5px 5px 5px;">
            <span>Total</span><br/>
            <span>--------</span><br/>
            <span>Done:${done}</span><br/>
            <span>Overdue:${overdue}</span><br/>
        </div>
        `
};

const productivityDataTransformer = (
    name: string,
    done: number,
    overdue: number,
    doneColor: string,
    overdueColor: string
): any[] => {
    const createTooltip = createCustomTooltip(done, overdue);
    const doneData = [done, createTooltip];
    const overdueData = [overdue, createTooltip];
    const productivityDataRow = [name, ...doneData, doneColor, ...overdueData, overdueColor];
    return productivityDataRow
};

const productivityDataJSON = [
    {
        name: 'Yana R',
        done: 23,
        doneColor: '#2196F4',
        overdue: 37,
        overdueColor: '#63B5F6'
    },
    {
        name: 'Olya B',
        done: 15,
        doneColor: '#673ab7',
        overdue: 12,
        overdueColor: '#9375CC'
    },
    {
        name: 'Luba K',
        done: 30,
        doneColor: '#4CAF4F',
        overdue: 11,
        overdueColor: '#81C784'
    },
];

const productivityData: any[] = productivityDataJSON.map(item => {
    const {name, done, overdue, doneColor, overdueColor} = item;
    return productivityDataTransformer(name, done, overdue, doneColor, overdueColor)
});

export const productivityChartData = [productivityColumns, ...productivityData];
