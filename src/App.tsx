import * as React from 'react';
import './App.css';

// import GoogleCumulative from './hoc/GoogleCumulative';
import {GoogleLineChart as GoogleCumulative} from './components/GoogleChart';
import {ChartjsChart} from './components/ChartjsChart';
import {ProductivityChart} from './components/ProductivityChart';
import logo from './logo.svg';

class App extends React.Component {
  public render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Charts reserach</h1>
        </header>
        <GoogleCumulative />
        <ChartjsChart />
        <ProductivityChart />
      </div>
    );
  }
}

export default App;
