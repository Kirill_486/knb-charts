import *as React from 'react';
import {Chart} from 'react-google-charts'
// import { ICumulativeFlowOptions } from 'src/types/googleCharts';
import { googleCumulativeData } from 'src/defaults/cumulativeFlowDefault';

const cumulativeOptions: any = {
    title: 'TemplateCumulativeChart',
    hAxis: {
        title: 'Cards',
        slantedText: true, 
        slantedTextAngle: -36
    },
    vAxis: {
        title: 'Time'        
    },
    tooltip: {
        isHtml: true
    },
    legend: 'none'
};


export const GoogleLineChart: React.SFC<{}> = () => {
    return (
        <div className="google-line__container">
            <h1>Google line chart is here</h1>
            <Chart
                width={'100%'}
                height={300}
                chartType="LineChart" 
                data={googleCumulativeData}
                options={cumulativeOptions}/>
        </div>
    );
};