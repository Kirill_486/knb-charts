import {connect} from 'react-redux';
import {GoogleLineChart} from '../components/GoogleChart';

const mapStateToProps = () => {
    return {}
}

const connectedGoogleChart = connect(mapStateToProps)(GoogleLineChart);
connectedGoogleChart.displayName = 'GoogleCumulativeFlow';

export default connectedGoogleChart;