export interface IGoogleChartsColumn {
    type: string;
    label?: string;
    role?: string;
    p?: {
        html: boolean
    }
};

export type IGoogleChartsRow = any[];

export interface ICumulativeFlowOptions {
    title: string;
    vAxis?: {
        title: string;
        minValue?: number;
    }
    hAxis?: {
        title: string;
        minValue?: number;
    },
    tooltip: {
        isHtml: boolean
    },
    legend: string
};
