import *as React from 'react';
import {Chart} from 'react-google-charts'
import {productivityChartData} from 'src/defaults/productivityChartDefault';

const productivityChartOptions: any = {
    title: 'ProductivityChart',
    hAxis: {
        title: 'Users',
        slantedText: true,
        slantedTextAngle: -36,
    },
    vAxis: {
        title: 'Cards',
    },
    tooltip: {
        isHtml: true
    },
    legend: 'none',
    isStacked: true,
    bar: {
        groupWidth: 16
    }
};


export const ProductivityChart: React.SFC<{}> = () => {
    return (
        <div className="google-line__container">
            <h1>Google column chart is here</h1>
            <Chart
                width={'100%'}
                height={600}
                chartType="ColumnChart"
                data={productivityChartData}
                options={productivityChartOptions}/>
        </div>
    );
};
